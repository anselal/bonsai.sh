# bonsai.sh

![](previews/bonsai.png)

`bonsai.sh` is a bonsai tree generator for the terminal, written in `bash`. It supports both live and static generation.

# Installation

To clone to `~/bin/bonsai.sh`, make executable, and add to available commands:

```bash
git clone https://gitlab.com/jallbrit/bonsai.sh ~/bin/bonsai.sh
chmod +x ~/bin/bonsai.sh/bonsai.sh
ln -s ~/bin/bonsai.sh/bonsai.sh ~/.local/bin/bonsai
```

# Usage

```
Usage: bonsai [-l [-t time]] [-m text] [-b int] [-i [-w time]] [-T]
                    [-g x,y] [-c char] [-M int] [-L int] [-s int] [-h]
bonsai.sh is a static and live bonsai tree generator, written in bash.

optional args:
  -l, --live             enable live mode: watch the tree grow
  -t, --time time        in live mode, time in secs between each step of growth [default: 0.01]
  -m, --message text     attach a message to the tree
  -b, --basetype int     which ascii-art plant base to use (0 for none) [default: 1]
  -i, --infinite         enable infinite mode: keep generating trees until quit
  -w, --wait sec         in infinite mode, time in secs between tree generation [default: 2]
  -T, --termcolors       use terminal colors
  -g, --geometry x,y     set custom geometry [default: fit to terminal]
  -c, --leaf char        character used for leaves [default: &]
  -M, --multiplier 0-20  branch multiplier; higher equals more branching [default: 5]
  -L, --life int 1-200   life of tree; higher equals more overall growth [default: 28]
  -s, --seed int         seed for tree; value (0-32767) is used to set the random number generator
  -h, --help             show help
```

# Examples

## `bonsai -l -b 2`

![bonsai -Ti](previews/bonsai-lb2.gif)

## `bonsai -T -m "$(fortune)"`

![bonsai -l](previews/bonsai-Tm.png)

# TODO

* Add a mode that grows based on percentage throughout minute/hour/day (e.g. in minute mode, when the clock hits 45s, the tree should be 75% done growing and restart when the next minute comes) (credit: u/Esko997)
* Optimization in live mode
* Add compatibility with Mac

# Inspiration

Originally, this script was simply a port of [this bonsai generator](http//andai.tv/bonsai/), written in JS, but has since evolved to include multiple CLI options significantly improved tree generation. `bonsai.sh` wouldn't be here if it weren't for that!
